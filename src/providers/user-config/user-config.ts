import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class UserConfigProvider {

  //App
  public appname: any = "Gardening";

  //configurations to setup the App
  public baseURL: any = "http://yourwordpress.com" + "/wp-json/wp/v2/";

  public privacyPolicyURL: any = "http://yourwordpress.com/privacy-policy/"; 
  
  public termsOfServcieURL: any = "http://yourwordpress.com/terms-and-conditions/"; 
  
  public parentcategory: any = "2";
  public defaultcategory: any = "5";

  //IAP
  public iapIDiOS = "yearlysubscription";
  public iapIDandroid = "gardening.yearly";
  public iapsalespostID: any = "38";
  public iaplockedContentTagID: any = "31";

  public GAnalyticsID: any = "UA-XXXXXXXX-XX";


  // Notification 
  public notificationCategoryID = "28";
  public notificationDelay = "1440"; //Should be in minutes


  // Ad Monetisation
  public adMobFrequency: any = 1; // One in every ____ times
  public adMobiOSInterestitialID: any = "ca-app-pub-XXXXXXXXXXXXXXXX/XXXXXXXXXXX";
  public adMobAndroidInterestitialID: any = "ca-app-pub-XXXXXXXXXXXXXXXX/XXXXXXXXXX";
  public admobTest = false;

  constructor(public http: Http) {
    console.log('Hello UserConfigProvider Provider');
  }

}
