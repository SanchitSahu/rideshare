import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Platform  } from 'ionic-angular';

import { InAppPurchase } from '@ionic-native/in-app-purchase';
import { Storage } from '@ionic/storage';


import {UserConfigProvider} from '../../providers/user-config/user-config';
// import { LocalNotifications } from '@ionic-native/local-notifications';
/**
 * Generated class for the SettingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage({
  name : 'setting',
  segment: 'setting'
})
@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {

  iapID: any;
  localstorage: any;
  products: any;
  loader: any;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public storage: Storage,
    public platform: Platform,
    public loading: LoadingController, 
    public userConfig: UserConfigProvider,
    // private localNotifications: LocalNotifications,
    private iap: InAppPurchase) {

      this.loader = this.loading.create();
      
            if (this.platform.is('ios')) {
              
                this.iapID = this.userConfig.iapIDiOS;
              
              }else if(this.platform.is('android')){
              
                this.iapID = this.userConfig.iapIDandroid;

              }

              this.localstorage = storage;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingPage');


    this.loader.present().then(() => {
      this.iap
      .getProducts([this.iapID])
      .then((products) => {
      
        this.products = products;
        
      })
      .catch((err) => {
        console.log(err);
      });
      this.loader.dismiss();
    });
  }

  notificationSwitch(){



  }

  restorePurchase(){

    // alert("hello");

    this.iap.restorePurchases().then(function (data) {
      console.log(data);

      if(data){
        this.localstorage.set('iap', true);
        this.navCtrl.pop();
      }
      /*
        [{
          transactionId: ...
          productId: ...
          state: ...
          date: ...
        }]
      */
    })
    .catch(function (err) {
      console.log(err);
    });

  }

}
