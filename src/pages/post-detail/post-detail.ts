import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { IonicPage, NavController, NavParams, LoadingController, Platform } from 'ionic-angular';
import 'rxjs/add/operator/map';
import {DomSanitizer} from '@angular/platform-browser';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Storage } from '@ionic/storage';

import {UserConfigProvider} from '../../providers/user-config/user-config';
import { AdMobFree } from '@ionic-native/admob-free';

/**
 * Generated class for the PostDetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage({
  name : 'post',
  segment: 'post/:postSlug',
  defaultHistory: ['home']
})
@Component({
  selector: 'page-post-detail',
  templateUrl: 'post-detail.html',
})
export class PostDetailPage {

  postdetail: any;
  postSlug: any;
  loader: any;
  postcontent: any;
  admob: any;
  adMobInterestitialID: any;
  baseURL: any;
  browser;
  iap: any = false;
  localstorage: any;
  admobFrequency: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public http: Http, 
    public loading: LoadingController, 
    private domSanitizer: DomSanitizer, 
    public admobFree: AdMobFree, 
    public platform: Platform, 
    public storage: Storage,
    public userConfig: UserConfigProvider, 
    private iab: InAppBrowser ) {

    this.baseURL = userConfig.baseURL;

    this.postSlug = this.navParams.get("postSlug");
    this.loader = this.loading.create();

    this.admobFrequency = userConfig.adMobFrequency;

    this.localstorage = storage;
    this.admob = admobFree;

    //set the device flag for cordova realted features on the view
    
    if(this.platform.is('cordova')){

      this.localstorage.get('iap').then((val) => {
        this.iap = val;
      });
  
      this.admob.interstitial.config({ 
        id: this.adMobInterestitialID,
        isTesting: this.userConfig.admobTest
      });
    
      if (this.platform.is('ios')) {
      
        this.adMobInterestitialID = userConfig.adMobiOSInterestitialID;
        console.log('Platform is ios');
      
      }else if(this.platform.is('android')){
      
        this.adMobInterestitialID = userConfig.adMobAndroidInterestitialID;
        console.log('Platform is android');
      
      }

    }
    
    
  }

  ionViewDidLoad(){
    // console.log('ionViewDidLoad PostDetailPage');
    console.log(this.postSlug);

    this.loader.present().then(() => {
  
      var url = this.baseURL+'posts?slug='+encodeURIComponent(this.postSlug)+'&_embed';
  
      console.log(url);
      
      this.http.get(url).map(res => res.json()).subscribe(data => {
       
        console.log(data);
        this.postdetail = data[0];
        
        this.postcontent = this.domSanitizer.bypassSecurityTrustHtml(this.postdetail.content.rendered);
          // this.postcontent = this.domSanitizer.bypassSecurityTrustStyle(this.postcontent);
        console.log(this.postcontent);
  
      });
      this.loader.dismiss();
    });

  }


  ionViewDidLeave(){

    if (this.platform.is('cordova') && !this.iap) { 

        //content is free and iap is not purchased
        var randomNumber = Math.floor((Math.random() * this.admobFrequency) + 1);
        if (randomNumber == 1){
          // console.log("Int Prepare");
          this.admob.interstitial.prepare();
        }
      }

  }

  openLink(link){
    this.browser = this.iab.create(link);
  }

}
