import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { IonicPage, NavController, NavParams, LoadingController, Platform } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Storage } from '@ionic/storage';

import {UserConfigProvider} from '../../providers/user-config/user-config';
import { AdMobFree } from '@ionic-native/admob-free';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

import { Network } from '@ionic-native/network';

@IonicPage({
  name : 'home',
  segment: 'home/:categorySlug'
})
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  posts: any;
  categoryID = "";
  categoryName = "";
  loader: any;
  baseURL: any;
  propertyURL: any;
  admob: any;
  adMobInterestitialID: any;
  admobFrequency: any;
  appname: any;
  connectSubscription;
  browser:any;
  notificationNotClickedFlag:any = true;
  isCordova = false;
  iap: any = false;
  localstorage: any;
  infiniteScroll = 1;
  
  constructor(
    public userConfig: UserConfigProvider ,
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public http: Http, 
    public loading: LoadingController, 
    public admobFree: AdMobFree, 
    public platform: Platform, 
    private iab: InAppBrowser, 
    public network: Network, 
    private ga: GoogleAnalytics,
    public storage: Storage,
    private localNotifications: LocalNotifications) {
    
    
    this.baseURL = userConfig.baseURL;
    this.appname = userConfig.appname;
    
    if(this.platform.is('cordova')){

      //set the device flag for cordova realted features on the view
      this.isCordova = true;
      
      if (this.platform.is('ios')) {
      
        this.adMobInterestitialID = userConfig.adMobiOSInterestitialID;
        console.log('Platform is ios');
      
      }else if(this.platform.is('android')){
      
        this.adMobInterestitialID = userConfig.adMobAndroidInterestitialID;
        console.log('Platform is android');
      
      }
    }

    console.log(this.isCordova);
    
    this.admobFrequency = userConfig.adMobFrequency;
    
    this.admob = admobFree;

    this.localstorage = storage;
    
  }
  
  ionViewDidLoad() {
    
    console.log('ionViewDidLoad PostDetailPage');
    
    // watch network for a connection
    this.connectSubscription = this.network.onConnect().subscribe(() => {
      console.log('network connected!');
      // We just got a connection but we need to wait briefly
      // before we determine the connection type. Might need to wait.
      // prior to doing any api requests as well.
      
      this.loadPosts();
      
      setTimeout(() => {
        if (this.network.type === 'wifi') {
          console.log('we got a wifi connection, woohoo!');
        }
      }, 3000);
    });
    
    
    this.ga.startTrackerWithId(this.userConfig.GAnalyticsID)
    .then(() => {
      console.log('Google analytics is ready now');
      this.ga.trackView('Catalog Page');
      // Tracker is ready
      // You can now track pages or set additional information such as AppVersion or UserId
    })
    .catch(e => console.log('Error starting GoogleAnalytics', e));
    
    
    this.loadPosts();
    
  }
  
  ionViewDidEnter(){
    
    if (this.platform.is('cordova')) {               
      
      console.log('platform is mobile');
      
      this.localstorage.get('iap').then((val) => {
        this.iap = val;
      });

      this.admob.interstitial.config({ 
        id: this.adMobInterestitialID,
        isTesting: this.userConfig.admobTest
      });
      
      
      var myself = this;
      this.localNotifications.on("click", function(notification) {
        
        //check to stop the notification call back from running twice
        if(myself.notificationNotClickedFlag){
          
          // console.log("This is notification data", notification.data);
          
          var unpackedData = JSON.parse(notification.data);
          var notificationPostSlug = unpackedData['postSlug'];
          
          var post = { slug: notificationPostSlug,
            format: 'standard'}
            myself.openPostDetail(post);
            
            myself.notificationNotClickedFlag = false;
            
          }
          
        });
        
      } 
      
    }
    
    ionViewDidLeave(){
      this.notificationNotClickedFlag = true;
    }

  
  loadPosts(){
    
    this.categoryID = this.navParams.get("categoryID");
    if(this.navParams.get("categoryName")){
      this.categoryName = this.navParams.get("categoryName");
    }else{
      this.categoryName = this.userConfig.appname;
    }

    this.loader = this.loading.create();
    
    if (this.categoryID){
      
      this.categoryID = this.navParams.get("categoryID");
      
    }else{
      
      this.categoryID = this.userConfig.defaultcategory;
          
        }
        var loadPostsURL = this.baseURL + "posts?_embed&categories=" + this.categoryID;
        
        this.loader.present().then(() => {
          
          this.http.get(loadPostsURL).map(res => res.json()).subscribe(data => {

            this.posts = this.processDataForPlatform(data);

            

          });

          this.loader.dismissAll();
          
        });

  }

  processDataForPlatform(data){

    if(this.platform.is('cordova')){
      
      return data;
      
    }else{
      
      var newArray = new Array();

      for (var index = 0; index < data.length; index++) {

        var lockedContentID = parseInt(this.userConfig.iaplockedContentTagID);

        if(data[index].tags.indexOf(lockedContentID) == -1){

          newArray.push(data[index]);

        }
                      
      }

      return newArray;
            
    }

  }
  
  openPostDetail(post){

    
    console.log("openPostDetails", post.slug);

    var tags = post.tags;
    
    var lockedContentID = parseInt(this.userConfig.iaplockedContentTagID);
    var paidContent = tags.indexOf(lockedContentID);
    console.log(this.userConfig.iaplockedContentTagID);
    console.log(paidContent);
    
    if (this.platform.is('cordova')) { 
      
      this.ga.trackEvent("Home Page", "Open Post Details", post.slug, 0, false);
      
      //if post is a premium content
      
      if(paidContent != -1){
        
        console.log("This is a paid content");
        
        if(this.iap){
          
          //content is paid and iap is purchaed
          this.navigateToPost(post);
          
        }else{
          
          //content is paid but iap is not purchased
          this.navCtrl.push("iap");
          
        }
        
      }else{
        
        if(this.iap){
          
          //content is free and iap is purchased
          this.navigateToPost(post);
          
        }else{
          
          //content is free and iap is not purchased
          var randomNumber = Math.floor((Math.random() * this.admobFrequency) + 1);
          if (randomNumber == 1){
            
            
            var myself = this;
    
            this.admob.interstitial.prepare().then(function (){
    
              myself.navigateToPost(post);
    
            });

          }

        }
        
      }
      
    }else{

      this.navigateToPost(post);

    } 

    
    
  }
  
  navigateToPost(post){
    
        if (post.format == 'link'){
          this.openLink(this.removeHTML(post.content.rendered));
          
        }else{
          this.navCtrl.push("post", {"postSlug":post.slug});
        }
    
  }


  showIntAd(){

    if (this.platform.is('cordova')) {     
      
      this.ga.trackEvent("Home Page", "Ad Button", "Action",0, false);
      
      this.admob.interstitial.prepare();

    } 

  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() => {

      var offset = this.infiniteScroll * 10;
      this.infiniteScroll = this.infiniteScroll + 1;

      var infiniteURL = this.baseURL + "posts?_embed&offset="+offset+"&categories=" + this.categoryID;
        
      this.http.get(infiniteURL).map(res => res.json()).subscribe(data => {

        this.posts.push.apply(this.posts, this.processDataForPlatform(data));


      });
      
      console.log('Async operation has ended');

      infiniteScroll.complete();
    }, 500);
  }

  removeHTML(text){
    return  text ? String(text).replace(/<[^>]+>/gm, '') : '';
  }

  openLink(link){
     this.browser = this.iab.create(link);
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.loadPosts();
      refresher.complete();
    }, 2000);
  }

}