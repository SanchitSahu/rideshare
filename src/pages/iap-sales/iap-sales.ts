import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { IonicPage, NavController, NavParams, LoadingController, Platform } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';
import { InAppPurchase } from '@ionic-native/in-app-purchase';
import { Storage } from '@ionic/storage';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import {UserConfigProvider} from '../../providers/user-config/user-config';
/**
 * Generated class for the IapSalesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage({
  name : 'iap',
  segment: 'iapsales',
  defaultHistory: ['home']
})
@Component({
  selector: 'page-iap-sales',
  templateUrl: 'iap-sales.html',
})
export class IapSalesPage {

  postdetail: any;
  postID: any;
  loader: any;
  postcontent: any;
  baseURL: any;
  iapID: any;
  browser;
  products: any;
  localstorage: any;
  privacyPolicyLink: any;
  termsOfServcieLink: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public http: Http, 
    public loading: LoadingController, 
    private domSanitizer: DomSanitizer, 
    public userConfig: UserConfigProvider,
    public platform: Platform,
    public storage: Storage,
    private iab: InAppBrowser,
    private iap: InAppPurchase) {
  
    this.baseURL = userConfig.baseURL;
    this.privacyPolicyLink = userConfig.privacyPolicyURL;
    this.termsOfServcieLink = userConfig.termsOfServcieURL;
    
    this.postID = this.navParams.get("postID");
    this.loader = this.loading.create();

    this.postID = this.userConfig.iapsalespostID;

    if (this.platform.is('ios')) {
    
      this.iapID = this.userConfig.iapIDiOS;
    
    }else if(this.platform.is('android')){
    
      this.iapID = this.userConfig.iapIDandroid;
    
    }

    this.localstorage = storage;
  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IapSalesPage');

    this.loader.present().then(() => {
      
      var url = this.baseURL+'posts/'+this.postID+'?_embed';
  
      // console.log(url);
      
      this.http.get(url).map(res => res.json()).subscribe(data => {
        
        this.postdetail = data;
        this.postcontent = this.domSanitizer.bypassSecurityTrustHtml(this.postdetail.content.rendered);
      
        console.log(this.postcontent);
  
      });

      this.loader.dismissAll();

      this.iap
      .getProducts([this.iapID])
      .then((products) => {
      
        this.products = products;
        
      })
      .catch((err) => {
        console.log(err);
      });
      
    });

  }
  
  buyProduct(){
    
    this.iap
    .subscribe(this.iapID)
    .then((data)=> {
      console.log(data);
      
      this.localstorage.set('iap', true);
      this.navCtrl.pop();
      
    })
    .catch((err)=> {
      console.log(err);
    });

  }

  privacyPolicy(){
    this.openLink(this.privacyPolicyLink);
  }

  termsOfService(){
    this.openLink(this.termsOfServcieLink);
  }

  openLink(link){
    this.browser = this.iab.create(link, 'location=yes');
  }

}
