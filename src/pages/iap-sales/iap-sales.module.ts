import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IapSalesPage } from './iap-sales';

@NgModule({
  declarations: [
    IapSalesPage,
  ],
  imports: [
    IonicPageModule.forChild(IapSalesPage),
  ],
})
export class IapSalesPageModule {}
