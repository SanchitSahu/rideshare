import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import 'rxjs/add/operator/map';

import { Network } from '@ionic-native/network';

import { LocalNotifications } from '@ionic-native/local-notifications';
import { Storage } from '@ionic/storage';

import {UserConfigProvider} from '../providers/user-config/user-config';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;

  rootPage: any = "home";
  menuName: any = "";
  parentcategory: any;
  GAnalyticsID: any;
  loader: any ;
  onesignalappID: any;
  disconnectSubscription;
  connectSubscription;
  notificationCateoryID;
  ionStorage: any;
  currentDay;
  iap: any;
  pages: Array<{title: string, component: any}>;
  baseURL: any;
  notificationDelay;

  constructor(public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen, 
    public http: Http, 
    public userConfig: UserConfigProvider, 
    public network: Network, 
    public loading: LoadingController,
    private localNotifications: LocalNotifications,
    public storage: Storage) {

    this.initializeApp();

    this.menuName = userConfig.appname;
    this.baseURL = userConfig.baseURL;
    this.parentcategory = userConfig.parentcategory;
    
    this.GAnalyticsID = userConfig.GAnalyticsID;
    this.notificationCateoryID = userConfig.notificationCategoryID;
    this.notificationDelay = parseInt(userConfig.notificationDelay);

    storage.get('day').then((val) => {
      
      if(val){ 
        this.currentDay = this.dayDifference(val, new Date());
      }else{
        this.currentDay = 0;
        storage.set('day', new Date());
      }
  
    });
    
    this.loadCategories();
    
  }
  
  initializeApp() {
    
    this.platform.ready().then(() =>{
      
      
      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(false);
      this.splashScreen.show();


      if (this.platform.is('cordova')) {  
        
        //clear all scheduled notifications
        this.localNotifications.clearAll();        
        this.localNotifications.cancelAll();
        
        this.http.get(this.baseURL+'posts?_embed&orderby=slug&order=asc&categories='+this.notificationCateoryID).map(res => res.json()).subscribe(data => {
          // console.log(data);
          if(data[this.currentDay]){
            
            //go through the notification posts and schedule the notifications
            for (var index = this.currentDay; index < data.length; index++) {
              
              //builds the data object index X day from now
              var adayfromnow = new Date(new Date().getTime() + ( ((index + 1) * this.notificationDelay) * 60 * 1000));

              
              //schedule the notification in the future for each post
              this.localNotifications.schedule({
                id: index,
                title: data[index].title.rendered,
                text: this.removeHTML(data[index].excerpt.rendered),
                at: adayfromnow,
                badge: 1,
                data: {'postSlug':data[this.currentDay].slug}
              });   

              console.log("notification set");

            }

            console.log(this.localNotifications.getAllScheduled());
          }

       });



        // cordova.plugins.notification.local.on("click", function(notification) {
          // alert("clicked: " + notification.id);

        // if(this.onesignalappID){ 
        //   this.oneSignal.startInit(this.onesignalappID, "YOUR_GOOGLE_PROJECT_NUMBER_IF_ANDROID");
        //   this.oneSignal.handleNotificationOpened().subscribe((jsonData) => {
        //     // do something when a notification is opened
        //     console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
        //   });
        //   this.oneSignal.endInit();
        // }

      }
      
      // watch network for a disconnect
      this.disconnectSubscription = this.network.onDisconnect().subscribe(() => {
        this.loader = this.loading.create();
        this.loader.present().then(() => {
          console.log('network was disconnected :-(');
          
        
        });
      });

      // watch network for a connection
      this.connectSubscription = this.network.onConnect().subscribe(() => {
        console.log('network connected!');
        // We just got a connection but we need to wait briefly
        // before we determine the connection type. Might need to wait.
        // prior to doing any api requests as well.
        
        this.loader.dismissAll();

        this.loadCategories();

        setTimeout(() => {
          if (this.network.type === 'wifi') {
            console.log('we got a wifi connection, woohoo!');
          }
        }, 3000);
      });
  
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
   
    });
  }

  dayDifference(from, to){

    var date2 = new Date(to);
    var date1 = new Date(from);
    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    var dayDifference = Math.floor(timeDiff / (1000 * 3600 * 24));
    // console.log(date2, date1);
    return dayDifference;

  }
    
  loadCategories(){

    this.http.get(this.baseURL+'categories?_embed&parent='+this.parentcategory).map(res => res.json()).subscribe(data => {
      this.pages = data;
   });

  }

  removeHTML(text){
    return  text ? String(text).replace(/<[^>]+>/gm, '') : '';
  }

  openCategory(categoryID, categorySlug, categoryName) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot("home", {"categoryID":categoryID, "categorySlug": categorySlug, "categoryName": categoryName});
  }

  openPage(page){

    this.nav.setRoot(page);

  }

}
