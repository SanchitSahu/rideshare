import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, DeepLinkConfig } from 'ionic-angular';
import { PhonegapLocalNotification } from '@ionic-native/phonegap-local-notification';
import { AdMobFree } from '@ionic-native/admob-free';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Network } from '@ionic-native/network';

import { LocalNotifications } from '@ionic-native/local-notifications';
import { IonicStorageModule } from '@ionic/storage';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { InAppPurchase } from '@ionic-native/in-app-purchase';

import { PathLocationStrategy, LocationStrategy } from '@angular/common';


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { PostDetailPage } from '../pages/post-detail/post-detail';
import { IapSalesPage } from '../pages/iap-sales/iap-sales';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { UserConfigProvider } from '../providers/user-config/user-config';

export const deepLinkConfig: DeepLinkConfig = {
  links: [
    { component: HomePage, name: 'home', segment: 'home/:categorySlug' },
    { component: PostDetailPage, name: 'post', segment: 'post/:postSlug', defaultHistory: ['home'] },
    { component: IapSalesPage, name: 'iap', segment: 'iapsales', defaultHistory: ['home'] }
  ]
};

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp, {
    }, deepLinkConfig),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserConfigProvider,
    PhonegapLocalNotification,
    AdMobFree,
    InAppBrowser,
    Network,
    LocalNotifications,
    GoogleAnalytics,
    InAppPurchase,
    // {provide: LocationStrategy, useClass: PathLocationStrategy}
  ]
})
export class AppModule {}
